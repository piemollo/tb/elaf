// ====== ====== Extended Linear Algebra for FreeFem

// ------ Lock
IFMACRO(!keyELAF)

// ------- fullA*fullB
func real[int,int] AdA(
		real[int,int] &A,
		real[int,int] &B
	){
	// --- Get size
	int aN = A.n;
	int aM = A.m;
	int bN = B.n;
	int bM = B.m;
	assert(aM == bN);
	
	// --- Comp
	real[int,int] res(aN,bM);
	for(int i=0; i<bM; i++)	res(:,i) = A*B(:,i);
	return res;
}

// ------ fullAt*fullB (symmetric)
func real[int,int] AdA(
		real[int,int] &A,
		real[int,int] &B,
		string flag
	){
	// --- Get size
	int aN = A.n;
	int aM = A.m;
	int bN = B.n;
	int bM = B.m;
	assert(aN == bN);
	
	// --- Comp
	real[int,int] res(aM,bM);
	for(int i=0; i<aM; i++){
		for(int j=0; j<=i; j++){
			res(i,j) = A(:,i)'*B(:,j);
			res(j,i) = res(i,j);
		}
	}
	return res;
}

// ------ sparseA*fullB
func real[int,int] MdA(
		matrix &A, 
		real[int,int] &B
	){
	// --- Get size
	int aN = A.n;
	int aM = A.m;
	int bN = B.n;
	int bM = B.m;
	assert(aM == bN);
	
	// --- Comp
	real[int,int] res(aN,bM);
	for(int i=0; i<bM; i++)	res(:,i) = A*B(:,i);
	return res;
}
 
// ------ fullB'*sparseA*fullB (with A symm)
func real[int,int] AtdMdA(
		matrix &A, 
		real[int,int] &B
	){
	// --- Get size
	int aN = A.n;
	int aM = A.m;
	int bN = B.n;
	int bM = B.m;
	assert(aM == bN);
	
	// --- spA*B
	real[int,int] temp(aN,bM);
	temp = MdA(A,B);
	
	// --- comp
	real[int,int] res(bM,bM);
	res = AdA(B,temp,"symmetric");
	return res;
}

// ------ Lock
macro keyELAF()0//
ENDIFMACRO

// ====== ======
