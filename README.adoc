# Extended Linear Algebra for FreeFem
:Author:    Pierre Mollo
:Email:     pierre.mollo@univ-reims.fr
:Date:      2021-12-20
:Revision:  2021-12-20

:toc:

## How to use it

This library is design for link:https://freefem.org/[`FreeFem v4.9`].
To use it, you need first to download this repository and use `include <path to dir>/ELAF.idp`.
You will then have access to the following functions.

## Function list

* xref:AdA[`AdA`]: Full matrix dot full matrix.
* xref:MdA[`MdA`]: Sparse matrix dot full matrix.
* xref:AtdMdA[`AtdMdA`]: Transposed full mat. A dot sparse mat. M dot full mat. A.

## Documentation

.AdA [[AdA]]
`AdA` computes the matrix product between two full matrix (means `real[int,int]` in FreeFem).

.Arguments
* `real[int,int] A`: Left hand matrix
* `real[int,int] B`: Right hand matrix
* `string Flag`: Optional argument in case of symmetric matrices

.Use
[source,C]
----
real[int,int] A = [[1., 2.], [2., 1.]];
real[int,int] B = [[3., 1.], [1., 3.]];
real[int,int] C = AdA(A,B);
real[int,int] C = AdA(A,B,"sym"); // If A and B are sym.
----

.MdA [[MdA]]
`MdA` computes the matrix product between sparse matrix (left) and
full matrix (right).

.Arguments
* `matrix A`: Left hand matrix
* `real[int,int] B`: Right hand matrix

.Use
[source,C]
----
matrix A = eye(2);
real[int,int] B = [[1., 2.], [2., 1.]];
real[int,int] C = MdA(A,B);
----

.AtdMdA [[AtdMdA]]
`AtdMdA` computes the matrix product between transposed a full matrix (left), a sparse matrix (middle) and the same full matrix (right).

:WARNING: The full matrix is supposed to be symmetric.

.Arguments
* `matrix A`: Left hand matrix
* `real[int,int] B`: Right hand matrix

.Use
[source,C]
----
matrix A = eye(2);
real[int,int] B = [[1., 2.], [2., 1.]];
real[int,int] C = AtdMdA(A,B); // B^t * A * B
----
